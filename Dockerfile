FROM jupyter/datascience-notebook

RUN \
 pip install psycopg2-binary \
 pip install sqlalchemy \
 pip install plotly
